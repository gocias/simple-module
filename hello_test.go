package simpmod

import "testing"

func TestHello(t *testing.T) {
	result := Hello()
	if result != ConstHello {
		t.Errorf("\"Hello()\" should return \"%s\" instead of \"%s\"", ConstHello, result)
	}
}
