# Simple Module
An example of a simple Go module.

## Usage
Add a dependency for a package.
```shell
go get gitlab.com/gocias/simple-module
go get gitlab.com/gocias/simple-module@v0.4.0
```

Use the "Hello" method from the module.
```go
package main

import (
	"fmt"
	simpmod "gitlab.com/gocias/simple-module"
)

func main() {
	fmt.Println(simpmod.Hello())
}
```
