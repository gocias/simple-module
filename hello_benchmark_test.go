package simpmod

import (
	"testing"
)

func BenchmarkHello(b *testing.B) {
	result := Hello()
	if result != ConstHello {
		b.Errorf("\"Hello()\" should return \"%s\" instead of \"%s\"", ConstHello, result)
	}
}
