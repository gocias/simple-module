package simpmod

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestHelloAssertEqual(t *testing.T) {
	assert.Equal(t, ConstHello, Hello(), "they should be equal")
}
